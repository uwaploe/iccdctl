// Package iccdctl controls the inVADER ICCD and intensifier via a serial
// interface. The communication protocol is documented in the "Syntronics
// DAGX Camera System Software ICD" (Syntronics No. DRS2007-JXXX-XX)
package iccdctl

import (
	"fmt"
	"io"
)

// Message package synchronization bytes
const (
	Sync1 byte = 0x9a
	Sync2 byte = 0xe2
)

// Message command codes
type Command byte

const (
	CmdIdle         Command = 0x0
	CmdReset                = 0x01
	CmdPing                 = 0x02
	CmdGetBuild             = 0x20
	CmdGetHwSn              = 0x21
	CmdGetStatus            = 0x22
	CmdGetVidAvg            = 0x23
	CmdSetCgain             = 0x41
	CmdSetIgain             = 0x42
	CmdSetPsrc              = 0x43
	CmdSetCmode             = 0x44
	CmdSetSyncSrc           = 0x45
	CmdSetLowPulse          = 0x51
	CmdSetMedPulse          = 0x52
	CmdSetHighPulse         = 0x53
	CmdGetCgain             = 0x81
	CmdGetIgain             = 0x82
	CmdGetPsrc              = 0x83
	CmdGetCmode             = 0x84
	CmdGetSyncSrc           = 0x85
	CmdGetPwidth            = 0x90
	CmdPong                 = 0xa2
	CmdValBuild             = 0xa4
	CmdValHwSn              = 0xa5
	CmdValStatus            = 0xa6
	CmdValCgain             = 0xb0
	CmdValIgain             = 0xb1
	CmdValPsrc              = 0xb2
	CmdValCmode             = 0xb3
	CmdValSyncSrc           = 0xb4
	CmdValLowPulse          = 0xb5
	CmdValMedPulse          = 0xb6
	CmdValHighPulse         = 0xb7
)

var TraceFunc func(string, ...interface{})

var msgResponses map[Command]Command = map[Command]Command{
	CmdPing:      CmdPong,
	CmdGetBuild:  CmdValBuild,
	CmdGetHwSn:   CmdValHwSn,
	CmdGetStatus: CmdValStatus,

	CmdGetCgain:   CmdValCgain,
	CmdGetIgain:   CmdValIgain,
	CmdGetPsrc:    CmdValPsrc,
	CmdGetCmode:   CmdValCmode,
	CmdGetSyncSrc: CmdValSyncSrc,

	CmdSetCgain:   CmdValCgain,
	CmdSetIgain:   CmdValIgain,
	CmdSetPsrc:    CmdValPsrc,
	CmdSetCmode:   CmdValCmode,
	CmdSetSyncSrc: CmdValSyncSrc,
}

type ChecksumError struct {
	expected, got byte
}

func (e *ChecksumError) Error() string {
	return fmt.Sprintf("Bad checksum; expected %02x, got %02x", e.expected, e.got)
}

// Message represents a command packet on the RS-485 bus
type Message struct {
	address byte
	Cmd     Command
	Payload []byte
}

// NewMessage creates a new Message struct with a 2-byte payload
func NewMessage(cmd Command, value uint16) Message {
	return Message{
		Cmd:     cmd,
		Payload: []byte{byte((value >> 8) & 0xff), byte(value & 0xff)},
	}
}

// Value returns the payload as a 2-byte integer
func (m Message) Value() uint16 {
	if len(m.Payload) != 2 {
		return 0
	}
	return (uint16(m.Payload[0]) << 8) | uint16(m.Payload[1])
}

// Csum calculates the message packet checksum
func (m Message) Csum() byte {
	sum := m.address
	sum += byte(len(m.Payload))
	sum += byte(m.Cmd)
	for _, b := range m.Payload {
		sum += b
	}
	return sum
}

// Len returns the total length of the Message
func (m Message) Len() int {
	return len(m.Payload) + 6
}

// Sender returns the bus address of the sender for incoming messages.
func (m Message) Sender() byte {
	return m.address
}

// Write serializes the message and writes it to an io.Writer
func (m Message) Write(w io.Writer) error {
	n := m.Len()
	buf := make([]byte, n)
	buf[0] = Sync1
	buf[1] = Sync2
	buf[2] = m.address
	buf[3] = uint8(len(m.Payload))
	buf[4] = byte(m.Cmd)
	copy(buf[5:], m.Payload)
	buf[n-1] = m.Csum()
	if TraceFunc != nil {
		TraceFunc("OUT: %v\n", buf)
	}
	_, err := w.Write(buf)
	return err
}

// Read constructs a Message by reading from an io.Reader
func (m *Message) Read(r io.Reader) error {
	buf := make([]byte, 5)
	char := []byte{0}

	// Search for the sync sequence
	for {
		_, err := r.Read(char)
		if err != nil {
			return err
		}
		buf[0] = buf[1]
		buf[1] = char[0]
		if buf[0] == Sync1 && buf[1] == Sync2 {
			break
		}
	}

	// Read the rest of the header
	_, err := io.ReadFull(r, buf[2:])
	if err != nil {
		return err
	}

	*m = Message{
		address: buf[2],
		Cmd:     Command(buf[4]),
		Payload: make([]byte, int(buf[3])),
	}

	// Read the message payload and checksum
	_, err = io.ReadFull(r, m.Payload)
	csum := []byte{0}
	_, err = r.Read(csum)

	if TraceFunc != nil {
		TraceFunc("IN: %v%v%v\n", buf, m.Payload, csum)
	}

	if m.Csum() != csum[0] {
		return &ChecksumError{expected: m.Csum(), got: csum[0]}
	}
	return nil
}

// Bus represents the RS-485 bus
type Bus struct {
	port     io.ReadWriter
	haveEcho bool
}

// NewBus returns a pointer to a Bus struct, haveEcho should be set to true if
// the RS-485 interface echoes characters back.
func NewBus(port io.ReadWriter, haveEcho bool) *Bus {
	return &Bus{port: port, haveEcho: haveEcho}
}

// Send transmits a message to an address
func (b *Bus) Send(m Message, address int) error {
	m.address = byte(address)
	if b.haveEcho {
		m.Write(b.port)
		return m.Read(b.port)
	}
	return m.Write(b.port)
}

// Recv receives the next message from the Bus
func (b *Bus) Recv() (Message, error) {
	var m Message
	err := m.Read(b.port)
	return m, err
}
