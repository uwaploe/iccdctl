package iccdctl

import (
	"bytes"
	"encoding/json"
	"reflect"
	"testing"
	"time"
)

func TestMessageIo(t *testing.T) {
	var b bytes.Buffer

	m1 := Message{
		Cmd:     CmdPing,
		Payload: []byte{0, 0},
	}
	m1.Write(&b)

	if b.Len() != m1.Len() {
		t.Fatalf("Bad message length; expected %d, got %d", m1.Len(), b.Len())
	}

	var m2 Message
	err := m2.Read(&b)
	if err != nil {
		t.Fatal(err)
	}

	if m1.Csum() != m2.Csum() {
		t.Errorf("Message mismatch; expected %#v, got %#v", m1, m2)
	}
}

func TestBus(t *testing.T) {
	var b bytes.Buffer

	m1 := Message{
		Cmd:     CmdPing,
		Payload: []byte{0, 0},
	}
	bus := NewBus(&b, false)

	err := bus.Send(m1, 7)
	if err != nil {
		t.Fatal(err)
	}

	m2, err := bus.Recv()
	if err != nil {
		t.Fatal(err)
	}

	if m1.Cmd != m2.Cmd {
		t.Errorf("Message mismatch; expected %#v, got %#v", m1, m2)
	}
}

func TestNewMessage(t *testing.T) {
	var b bytes.Buffer
	bus := NewBus(&b, false)

	m := NewMessage(CmdSetLowPulse, 0x01b6)
	bus.Send(m, 7)
	pkt := b.Bytes()
	n := len(pkt)

	if pkt[n-1] != 0x11 {
		t.Errorf("Bad checksum; expected 0x11, got 0x%02x", pkt[n-1])
	}

	m2, err := bus.Recv()
	if err != nil {
		t.Fatal(err)
	}
	if m2.Value() != 0x01b6 {
		t.Errorf("Bad payload value; expected 0x01b6, got %#x", m2.Value())
	}
}

func TestPulseWidth(t *testing.T) {
	table := []struct {
		cmd    Command
		val    uint16
		result time.Duration
	}{
		{cmd: CmdValLowPulse, val: 0x1b6, result: time.Nanosecond * 4380},
		{cmd: CmdValMedPulse, val: 0x1b6, result: time.Microsecond * 439},
		{cmd: CmdValHighPulse, val: 0x1b6, result: time.Microsecond * 43900},
	}

	for _, e := range table {
		m := NewMessage(e.cmd, e.val)
		pw := pulseWidth(m)
		if pw != e.result {
			t.Errorf("Value mismatch; expected %s, got %s", e.result, pw)
		}
	}
}

func TestSetPulseWidth(t *testing.T) {
	table := []struct {
		cmd    Command
		val    uint16
		result time.Duration
	}{
		{cmd: CmdSetLowPulse, val: 0x1b6, result: time.Nanosecond * 4380},
		{cmd: CmdSetMedPulse, val: 0x1b6, result: time.Microsecond * 439},
		{cmd: CmdSetHighPulse, val: 0x1b6, result: time.Microsecond * 43900},
	}

	for _, e := range table {
		m := setPulseWidth(e.result)
		if m.Cmd != e.cmd {
			t.Errorf("Bad message type; expected %#x, got %#x", e.cmd, m.Cmd)
		}
		val := m.Value()
		if val != e.val {
			t.Errorf("Bad message payload; expected %#x, got %#x", e.val, val)
		}
	}
}

func TestGateJSON(t *testing.T) {
	table := []struct {
		in  string
		out Gate
		err error
	}{
		{in: `"external"`, out: Gate{Type: ExternalGate}},
		{in: `"42us"`, out: Gate{Type: InternalGate, PulseWidth: time.Microsecond * 42}},
		{in: `"foo"`, out: Gate{Type: Invalid}, err: ErrGate},
	}

	for _, e := range table {
		g := Gate{}
		err := json.Unmarshal([]byte(e.in), &g)
		if err != e.err {
			t.Errorf("Unexpected error value: %v", err)
		}
		if !reflect.DeepEqual(g, e.out) {
			t.Errorf("Bad value; expected %#v, got %#v", e.out, g)
		}
	}
}

func TestSyncJSON(t *testing.T) {
	table := []struct {
		in  string
		out SyncSource
		err error
	}{
		{in: `"external"`, out: ExternalSync},
		{in: `"internal"`, out: InternalSync},
		{in: `"foo"`, err: ErrSync},
	}

	for _, e := range table {
		var s SyncSource
		err := json.Unmarshal([]byte(e.in), &s)
		if err != e.err {
			t.Errorf("Unexpected error value: %v", err)
		}
		if s != e.out {
			t.Errorf("Bad value; expected %#v, got %#v", e.out, s)
		}
	}
}
