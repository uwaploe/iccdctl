package iccdctl

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/pkg/errors"
)

type GateSource int

const (
	Invalid GateSource = iota
	ExternalGate
	InternalGate
)

var ErrGate = errors.New("Invalid gate value")

// Gate describes the Intensifier gate configuration
type Gate struct {
	Type       GateSource
	PulseWidth time.Duration
}

func (g Gate) String() string {
	switch g.Type {
	case ExternalGate:
		return "external"
	case InternalGate:
		return g.PulseWidth.String()
	}
	return "invalid"
}

// MarshalJSON implements the json.Marshaler interface
func (g Gate) MarshalJSON() ([]byte, error) {
	return json.Marshal(g.String())
}

// UnmarshalJSON implements the json.Unmarshaler interface
func (g *Gate) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}

	switch s {
	case "external":
		g.Type = ExternalGate
		g.PulseWidth = 0
	default:
		tmp, err := time.ParseDuration(s)
		if err != nil {
			g.Type = Invalid
			g.PulseWidth = 0
			return ErrGate
		}
		g.Type = InternalGate
		g.PulseWidth = tmp
	}

	return nil
}

func (g *Gate) UnmarshalText(text []byte) error {
	s := string(text)
	switch s {
	case "external":
		g.Type = ExternalGate
		g.PulseWidth = 0
	default:
		tmp, err := time.ParseDuration(s)
		if err != nil {
			g.Type = Invalid
			g.PulseWidth = 0
			return ErrGate
		}
		g.Type = InternalGate
		g.PulseWidth = tmp
	}

	return nil
}

type SyncSource int

const (
	InternalSync SyncSource = iota
	ExternalSync
)

var ErrSync = errors.New("Invalid sync value")

func (s SyncSource) String() string {
	switch s {
	case InternalSync:
		return "internal"
	case ExternalSync:
		return "external"
	}

	return "invalid"
}

// MarshalJSON implements the json.Marshaler interface
func (s SyncSource) MarshalJSON() ([]byte, error) {
	return json.Marshal(s.String())
}

// UnmarshalJSON implements the json.Unmarshaler interface
func (s *SyncSource) UnmarshalJSON(b []byte) error {
	var v string
	if err := json.Unmarshal(b, &v); err != nil {
		return err
	}

	switch v {
	case "external":
		*s = ExternalSync
	case "internal":
		*s = InternalSync
	default:
		return ErrSync
	}

	return nil
}

func (s *SyncSource) UnmarshalText(text []byte) error {
	v := string(text)
	switch v {
	case "external":
		*s = ExternalSync
	case "internal":
		*s = InternalSync
	default:
		return ErrSync
	}

	return nil
}

// Send the contents of a message to an address ton the Bus and return
// the response.
func sendRecv(bus *Bus, addr int, m Message) (Message, error) {
	var resp Message
	err := bus.Send(m, addr)
	if err != nil {
		return resp, errors.Wrap(err, "send message")
	}
	return bus.Recv()
}

// Return the intensifier pulse width from the contents of a Message
func pulseWidth(m Message) time.Duration {
	val := int64(m.Value())
	switch m.Cmd {
	case CmdValLowPulse:
		return time.Duration(val) * time.Nanosecond * 10
	case CmdValMedPulse:
		return time.Duration(val+1) * time.Microsecond
	case CmdValHighPulse:
		return time.Duration((val+1)*100) * time.Microsecond
	}

	return time.Duration(0)
}

// Create a Message to set a new pulse width value
func setPulseWidth(pw time.Duration) Message {
	var m Message
	if pw < (time.Microsecond * 10) {
		ns := int64(pw / time.Nanosecond)
		m = NewMessage(CmdSetLowPulse, uint16(ns/10))
	} else if pw < time.Millisecond {
		us := int64(pw / time.Microsecond)
		m = NewMessage(CmdSetMedPulse, uint16(us-1))
	} else {
		us := int64(pw / time.Microsecond)
		m = NewMessage(CmdSetHighPulse, uint16((us-100)/100))
	}

	return m
}

// Intensifier represents the ICCD image intensifier device.
type Intensifier struct {
	bus  *Bus
	addr int
}

// Return an new Intensifier at a specified bus address
func NewIntensifier(bus *Bus, address int) *Intensifier {
	return &Intensifier{
		bus:  bus,
		addr: address,
	}
}

// SetGate sets the gate source and pulse width of the Intensifier gate
func (i *Intensifier) SetGate(g Gate) error {
	m := NewMessage(CmdSetPsrc, uint16(g.Type))

	resp, err := sendRecv(i.bus, i.addr, m)
	if err != nil {
		return errors.Wrap(err, "set source")
	}
	if resp.Cmd != CmdValPsrc {
		return fmt.Errorf("Invalid response")
	}

	if g.Type != InternalGate {
		return nil
	}

	m = setPulseWidth(g.PulseWidth)
	resp, err = sendRecv(i.bus, i.addr, m)
	if err != nil {
		return errors.Wrap(err, "set pulse width")
	}

	return nil
}

// Gate returns the current gate settings
func (i *Intensifier) Gate() (Gate, error) {
	var g Gate

	m := NewMessage(CmdGetPsrc, 0)
	resp, err := sendRecv(i.bus, i.addr, m)
	if err != nil {
		return g, err
	}

	g.Type = GateSource(resp.Value())
	if g.Type == Invalid || g.Type == ExternalGate {
		return g, nil
	}

	m = NewMessage(CmdGetPwidth, 0)
	resp, err = sendRecv(i.bus, i.addr, m)
	if err != nil {
		return g, err
	}

	g.PulseWidth = pulseWidth(resp)
	return g, nil
}

// SetGain sets the Intensifier gain as a fraction of the maximum gain
// value, 0 <= scale <= 1.0
func (i *Intensifier) SetGain(scale float32) error {
	val := uint16(scale * 1023)
	if val > 1023 {
		val = 1023
	}
	m := NewMessage(CmdSetIgain, val)
	_, err := sendRecv(i.bus, i.addr, m)
	return err
}

// Gain returns the Intensifier gain as a fraction of the maximum gain value
func (i *Intensifier) Gain() (float32, error) {
	m := NewMessage(CmdGetIgain, 0)
	resp, err := sendRecv(i.bus, i.addr, m)
	if err != nil {
		return 0, err
	}
	return float32(resp.Value()) / 1023.0, nil
}

// SetSync sets the Intensifier sync (trigger) source
func (i *Intensifier) SetSync(s SyncSource) error {
	m := NewMessage(CmdSetSyncSrc, uint16(s))
	_, err := sendRecv(i.bus, i.addr, m)
	return err
}

// Sync returns the current Intensifier sync source
func (i *Intensifier) Sync() (SyncSource, error) {
	m := NewMessage(CmdGetSyncSrc, 0)
	resp, err := sendRecv(i.bus, i.addr, m)
	if err != nil {
		return 0, err
	}
	return SyncSource(resp.Value()), nil
}
